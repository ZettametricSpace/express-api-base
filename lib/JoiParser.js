const getTypeMetadata =
exports.getTypeMetadata = description => {
    let output = {};

    if(description.flags) {
        if(description.flags.presence === "required")
            output.required = true;
    }

    return output;
}

const describeObject =
exports.describeObject = obj => {
    let desc = obj.describe ? obj.describe() : obj;

    switch(desc.type) {
        case "object": return {
            type: "object",
            properties: Object.keys(desc.keys).reduce((obj, key) => ({
                ...obj,
                [key]: describeObject(desc.keys[key])
            }), {}),
            ...getTypeMetadata(desc)
        };

        case "array": return {
            type: "array",
            children: desc.items.map(x => describeObject(x)),
            ...getTypeMetadata(desc)
        };

        case "string": return {
            type: "string",
            ...getTypeMetadata(desc)
        };

        case "number": return {
            type: "number",
            ...getTypeMetadata(desc)
        };

        case "boolean": return {
            type: "boolean",
            ...getTypeMetadata(desc)
        };

        case "date": return {
            type: "date",
            ...getTypeMetadata(desc)
        }
    }
}

const extractProperties =
exports.extractProperties = obj => Object.keys(obj).reduce((v, k) => {
    let desc = obj[k].describe();

    return {
        ...v,
        [k]: Object.keys(desc.keys).reduce((a, k) => ({
            ...a,
            [k]: describeObject(desc.keys[k])
        }), {})
    };
}, {});

const serializeSchema =
exports.serializeSchema = obj => Object.keys(obj).reduce((v, k) => {
    let desc = obj[k].describe();

    return {
        ...v,
        [k]: Object.keys(desc.keys).reduce((a, k) => ({
            ...a,
            [k]: desc.keys[k]
        }), {})
    };
}, {});