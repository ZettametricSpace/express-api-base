const buildConfig = require("express-api-base/utils/configBuilder");

module.exports = buildConfig({
    basePath: "/v1",
    api: {
        port: 8080
    },
    cors: {
        origins:  [ "http://*.localhost:3000", "https://*.prod.ui.com/" ]
    }
});
