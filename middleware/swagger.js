const OpenAPIBuilder = require("../lib/OpenAPIBuilder");
const SwaggerUI = require("swagger-ui-express");
const { Router } = require("express");
const YAML = require("yaml");

/**
 * TODO: Maybe cache/serve file, compare hashes to clean up memory? (generates a big object when parsing express app, would be nice to gc it)
 */
class Definition {
    constructor(app) {
        this.def = OpenAPIBuilder({ app: app, dest: "memory" });
    }

    get json() {
        if(!this._json) this._json = JSON.stringify(this.def, null, 4);

        return this._json;
    }

    get yaml() {
        if(!this._yaml) this._yaml = YAML.stringify(this.def);

        return this._yaml;
    }

    get raw() {
        return this.def;
    }
}

module.exports = app => {
    let definition = new Definition(app);
    let middleware = SwaggerUI.setup(definition.raw);

    app.use("/docs", SwaggerUI.serve);
    app.use("/docs", (req, res, next) => middleware(req, res, next));

    const router = Router();
    router.get("/swagger", (req, res) => {
        if(req.query["format"] === "yaml") { // YAML
            return res.status(200).header("Content-Type", "text/plain").end(definition.yaml);
        } else if(req.query["format"] === "json") { // Pretty JSON
            return res.status(200).header("Content-Type", "application/json").end(definition.json);
        } else { // Raw JSON
            return res.status(200).send(definition.raw);
        }
    });

    router.ignore = true;

    return router;
};
