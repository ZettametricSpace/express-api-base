declare function ConfigBuilder<T>(config: T): T;
ConfigBuilder.rc = ConfigBuilder;
ConfigBuilder.yaml = ConfigBuilder;

export default ConfigBuilder;
